define('questionaire/tests/helpers/start-app', ['exports', 'ember', 'questionaire/app', 'questionaire/config/environment'], function (exports, _ember, _questionaireApp, _questionaireConfigEnvironment) {
  exports['default'] = startApp;

  function startApp(attrs) {
    var attributes = _ember['default'].merge({}, _questionaireConfigEnvironment['default'].APP);
    attributes = _ember['default'].merge(attributes, attrs); // use defaults, but you can override;

    return _ember['default'].run(function () {
      var application = _questionaireApp['default'].create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
      return application;
    });
  }
});