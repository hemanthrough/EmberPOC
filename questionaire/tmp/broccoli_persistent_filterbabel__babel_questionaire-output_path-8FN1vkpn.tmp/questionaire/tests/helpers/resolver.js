define('questionaire/tests/helpers/resolver', ['exports', 'questionaire/resolver', 'questionaire/config/environment'], function (exports, _questionaireResolver, _questionaireConfigEnvironment) {

  var resolver = _questionaireResolver['default'].create();

  resolver.namespace = {
    modulePrefix: _questionaireConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _questionaireConfigEnvironment['default'].podModulePrefix
  };

  exports['default'] = resolver;
});