define('questionaire/tests/test-helper', ['exports', 'questionaire/tests/helpers/resolver', 'ember-qunit'], function (exports, _questionaireTestsHelpersResolver, _emberQunit) {

  (0, _emberQunit.setResolver)(_questionaireTestsHelpersResolver['default']);
});