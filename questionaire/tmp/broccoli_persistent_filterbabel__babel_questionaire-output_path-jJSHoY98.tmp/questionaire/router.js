define('questionaire/router', ['exports', 'ember', 'questionaire/config/environment'], function (exports, _ember, _questionaireConfigEnvironment) {

  var Router = _ember['default'].Router.extend({
    location: _questionaireConfigEnvironment['default'].locationType,
    rootURL: _questionaireConfigEnvironment['default'].rootURL
  });

  Router.map(function () {
    this.route('questions');
  });

  exports['default'] = Router;
});