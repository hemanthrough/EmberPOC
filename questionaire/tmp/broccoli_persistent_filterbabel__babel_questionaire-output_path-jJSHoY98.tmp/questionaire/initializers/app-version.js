define('questionaire/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'questionaire/config/environment'], function (exports, _emberCliAppVersionInitializerFactory, _questionaireConfigEnvironment) {
  var _config$APP = _questionaireConfigEnvironment['default'].APP;
  var name = _config$APP.name;
  var version = _config$APP.version;
  exports['default'] = {
    name: 'App Version',
    initialize: (0, _emberCliAppVersionInitializerFactory['default'])(name, version)
  };
});