define('questionaire/routes/questions', ['exports', 'ember'], function (exports, _ember) {

  var questions = [{
    id: 1,
    title: 'Grand Old Mansion',
    owner: 'Veruca Salt',
    city: 'San Francisco',
    type: 'Estate',
    bedrooms: 15,
    image: 'https://upload.wikimedia.org/wikipedia/commons/c/cb/Crane_estate_(5).jpg'
  }, {
    id: 2,
    title: 'Urban Living',
    owner: 'Mike TV',
    city: 'Seattle',
    type: 'Condo',
    bedrooms: 1,
    image: 'https://upload.wikimedia.org/wikipedia/commons/0/0e/Alfonso_13_Highrise_Tegucigalpa.jpg'
  }, {
    id: 3,
    title: 'Downtown Charm',
    owner: 'Violet Beauregarde',
    city: 'Portland',
    type: 'Apartment',
    bedrooms: 3,
    image: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Wheeldon_Apartment_Building_-_Portland_Oregon.jpg'
  }];

  exports['default'] = _ember['default'].Route.extend({
    actions: {
      willTransition: function willTransition(transition) {
        console.log("000000000000000000000000000000000000000000000000000000000000");
        if (this.controller.get('userHasEnteredData') && !confirm('Are you sure you want to abandon progress?')) {
          transition.abort();
        } else {
          // Bubble the `willTransition` action so that
          // parent routes can decide whether or not to abort.
          return false;
        }
      }
    },
    model: function model() {
      return questions;
    }

  });
});