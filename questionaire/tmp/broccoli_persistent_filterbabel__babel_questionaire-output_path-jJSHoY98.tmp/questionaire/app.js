define('questionaire/app', ['exports', 'ember', 'questionaire/resolver', 'ember-load-initializers', 'questionaire/config/environment'], function (exports, _ember, _questionaireResolver, _emberLoadInitializers, _questionaireConfigEnvironment) {

  var App = undefined;

  _ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = _ember['default'].Application.extend({
    modulePrefix: _questionaireConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _questionaireConfigEnvironment['default'].podModulePrefix,
    Resolver: _questionaireResolver['default']
  });

  (0, _emberLoadInitializers['default'])(App, _questionaireConfigEnvironment['default'].modulePrefix);

  exports['default'] = App;
});