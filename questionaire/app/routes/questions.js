import Ember from 'ember';

let questions = [{
  id: 1,
  title: 'what do you prefer ?',
  a1: 'Grand Old Mansion',
  a2: 'Veruca Salt',
  a3: 'San Francisco',
  type: 'mcq',
  a4: 15
}, {
  id: 2,
  title: 'What is your opinion on Urban Living',
  type: 'freetext',
}, {
  id: 3,
  title: 'Downtown Charm',
  a1: 'Violet Beauregarde',
  a2: 'Portland',
  type: 'checkbox',
  a4: 3,
  a3:'new'
}];

let theCurrentItem = {
                   id: 1,
                   title: 'Grand Old Mansion',
                   owner: 'Veruca Salt',
                   city: 'San Francisco',
                   type: 'Estate',
                   bedrooms: 15,
                   image: 'https://upload.wikimedia.org/wikipedia/commons/c/cb/Crane_estate_(5).jpg'
                 }

export default Ember.Route.extend({
    actions:{
        willTransition(transition) {
          console.log("000000000000000000000000000000000000000000000000000000000000")
          if (this.controller.get('userHasEnteredData') &&
              !confirm('Are you sure you want to abandon progress?')) {
            transition.abort();
          } else {
            // Bubble the `willTransition` action so that
            // parent routes can decide whether or not to abort.
            return false;
          }
        }

          },
  model() {

    return questions;
  }

  , currentItem(){
      console.log("bhkkkkkkkkkkk")
      return theCurrentItem;
    }

});
